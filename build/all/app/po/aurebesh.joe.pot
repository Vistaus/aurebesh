# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the aurebesh.joe package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: aurebesh.joe\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-19 22:51+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AboutApp.qml:31
msgid "About"
msgstr ""

#: ../qml/AboutApp.qml:47
msgid "About Aurebesh"
msgstr ""

#: ../qml/AboutApp.qml:61
msgid "Version: "
msgstr ""

#: ../qml/AboutApp.qml:65
msgid ""
"Practice reading Aurebesh words from the Star Wars universe. Input the word "
"that you see on the screen and press 'Check' to see if it is correct. Press "
"'Skip' to test a different word instead."
msgstr ""

#: ../qml/Main.qml:33 ../qml/Home.qml:32 aurebesh.joe.desktop.in.h:1
msgid "Aurebesh"
msgstr ""

#: ../qml/Home.qml:55
msgid "Switch to Letters"
msgstr ""

#: ../qml/Home.qml:55
msgid "Switch to Words"
msgstr ""

#: ../qml/Home.qml:88 ../qml/Home.qml:137
msgid "Do. Or do not."
msgstr ""

#: ../qml/Home.qml:94
msgid "Streak: "
msgstr ""

#: ../qml/Home.qml:100
msgid "Impressive."
msgstr ""

#: ../qml/Home.qml:101
msgid "There is no try."
msgstr ""

#: ../qml/Home.qml:108
msgid "Check"
msgstr ""

#: ../qml/Home.qml:115
msgid "Skip"
msgstr ""

#. TRANSLATORS: Description of the menu item
#: ../qml/Chart.qml:31 ../qml/modules/DefaultHeader.qml:37
msgid "Aurebesh Chart"
msgstr ""

#. TRANSLATORS: Description of the menu item
#: ../qml/Translator.qml:32 ../qml/modules/DefaultHeader.qml:43
msgid "Translator"
msgstr ""

#: ../qml/Translator.qml:50
msgid "Enter Text"
msgstr ""

#: ../qml/Translator.qml:616
msgid "Clear All"
msgstr ""

#: ../qml/Translator.qml:625
msgid "Space"
msgstr ""

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:49
msgid "About This App"
msgstr ""

#: ../qml/modules/DefaultHeader.qml:54
msgid "Report Bugs"
msgstr ""

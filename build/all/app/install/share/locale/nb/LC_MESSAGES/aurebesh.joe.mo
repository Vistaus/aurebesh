��          �      l      �     �     �     �                 	   #     -  
   <     G  �   S                %     +     4     F     V  
   g  	   r  =  |     �     �     �     �     �     �  	   �               %  �   1     �  	     	             !     4     A  
   Z  	   e     
                                                              	                               About About Aurebesh About This App Aurebesh Aurebesh Chart Check Clear All Do. Or do not. Enter Text Impressive. Practice reading Aurebesh words from the Star Wars universe. Input the word that you see on the screen and press 'Check' to see if it is correct. Press 'Skip' to test a different word instead. Report Bugs Skip Space Streak:  Switch to Letters Switch to Words There is no try. Translator Version:  Project-Id-Version: aurebesh.joe
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-01-18 19:38+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Om Om Aurebesh Om denne appen Aurebesh Aurebesh diagram Sjekk Slett alt Gjøre. Eller ikke. Tast inn tekst Imponerende Øv deg på å lese Aurebesh-ord fra Star Wars-universet. Skriv inn ordet som du ser på skjermen, og trykk 'Sjekk' for å se om det er riktig. Trykk 'Hopp over' for å teste et annet ord i stedet. Rappporter feil Hopp over Mellomrom Strek Bytt til bokstaver Bytt til ord Det er ikke noe forsøk. Oversetter Versjon:  